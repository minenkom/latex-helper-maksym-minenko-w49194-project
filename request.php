<?php
if ( !isset($_POST['source']) || $_POST['source'] === "" )
{
    header("location: index.html");
}

$file = "public/" . strtotime(date('Y-m-d h:i:s')) . ".tex";
$source = str_replace("<?php", "", $_POST['source']);
file_put_contents($file, $source);

if ( $curl = curl_init() )
{
    curl_setopt($curl, CURLOPT_URL, 'https://www.writelatex.com/docs');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "snip_uri=http://latex.tandmstudio.com/" . $file);
    $out = curl_exec($curl);
    if ( $out )
    {
        preg_match("/<a href=\"(.*)\">(.*)<\/a>/", $out, $m);
        if ( isset($m[1]) )
        {
            $parts = explode("/", $m[1]);
            curl_setopt($curl, CURLOPT_URL, (string) "https://www.writelatex.com/docs/" . $parts[count($parts) - 1] . "/pdf");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            
            $out = 105;
            while ( strlen($out) < 110 )
            {
                sleep(20);
                $out = curl_exec($curl);
                var_dump($out);
            }
            
            $test = explode('"', $out);
            $file_url = $test[1];
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="test"');
            header('Content-Length: ' . filesize($file_url));
            @readfile($file_url);
        }
    }
    curl_close($curl);
}